package org.example.service;

import org.example.entity.User;
import org.example.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class UserServiceTest {

    @Mock
    UserRepository userRepository;

    @InjectMocks
    UserService userService;

    @BeforeEach
    public void setUp(){
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetUserNameByIdSuccess(){
        //Expectated
        User user = new User(1, "Mas Jono");
        Mockito.when(userRepository.findById(1)).thenReturn(user);

        //Actual
        String userName = userService.getUserNameById(1);

        //Assertion Expectated
        Assertions.assertEquals(user.getName(),userName);
    }
}
