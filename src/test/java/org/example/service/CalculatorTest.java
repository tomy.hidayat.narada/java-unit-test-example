package org.example.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculatorTest {

    Calculator calculator = new Calculator();

    static Integer a = 1;
    static Integer b = 1;

    @Test
    void addTestPositive(){
        Integer expectation = 2;
        Integer actual = calculator.add(a,b);

        Assertions.assertEquals(expectation,actual);
    }

    @Test
    void subtractTestPositive(){
        Integer expectation = 0;
        Integer actual = calculator.subtract(a,b);

        Assertions.assertEquals(expectation,actual);
    }

    @Test
    void multiplyTestPositive(){
        Integer expectation = 1;
        Integer actual = calculator.multiply(a,b);

        Assertions.assertEquals(expectation,actual);
    }
}
