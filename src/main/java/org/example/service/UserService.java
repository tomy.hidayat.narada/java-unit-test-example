package org.example.service;

import org.example.entity.User;
import org.example.repository.UserRepository;

public class UserService {
    private UserRepository userRepository;

    public UserService(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    public String getUserNameById(Integer id){
        User user = userRepository.findById(id);
        return user != null ? user.getName() : "Unknown";
    }
}
